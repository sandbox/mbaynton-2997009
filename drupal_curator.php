<?php

/**
 * @var \Curator\AppManager $appManager
 */
$appManager = require __DIR__ . DIRECTORY_SEPARATOR . 'drupal_curator.phar';

$appManager->run();
